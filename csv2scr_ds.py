#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
csv2scr_ds.py
Ce programme convertit un fichier CSV en script SCR pour DraftSight.
Licence GPLv3
https://www.gnu.org/licenses/gpl-3.0.fr.html
2019/10/24
Jean-Charles Braun
'''

from tkinter import Tk, Canvas, Button, CENTER, YES, BOTH
from PIL import Image, ImageTk
import tkinter.constants
import tkinter.filedialog
import os
import csv
import math

# Définition des variables
filetext = 'Choix du fichier CSV'
dirtext = 'Répertoire cible'

def openFile():
    filename = tkinter.filedialog.askopenfilename(parent=root, initialdir='~', title=filetext, filetypes=[('CSV files', '.csv'), ('All files', '*')])
    if bool(filename):
        filebut['text'] = str(filename)
    else:
        filebut['text'] = filetext
    board.delete('all')
    board.update()
    board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)

def openDirectory():
    dirname = tkinter.filedialog.askdirectory(parent=root, initialdir='~', title=dirtext)
    if bool(dirname):
        dirbut['text'] = str(dirname)
    else:
        dirbut['text'] = dirtext
    board.delete('all')
    board.update()
    board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)

def InsNumAxes(startpoint, endpoint, info):
    x1 = float(startpoint.split(',')[0])
    x2 = float(endpoint.split(',')[0])
    y1 = float(startpoint.split(',')[1])
    y2 = float(endpoint.split(',')[1])
    z1 = float(startpoint.split(',')[2])
    z2 = float(endpoint.split(',')[2])
    midpoint_x = format((x1 + x2) / 2, '.3f')
    midpoint_y = format((y1 + y2) / 2, '.3f')
    midpoint_z = format((z1 + z2) / 2, '.3f')
    arctan = math.atan((y2 - y1) / (x2 - x1))
    angle = format(math.degrees(arctan), '.3f')
    fichier_scr.write('-NOTESIMPLE ' + midpoint_x + ',' + midpoint_y + ',' + midpoint_z + ' 0.05 ' + angle + ' ' + info + '\n')

def FileZDB(name_zdb, data_zdb):
    fichier_zdb = open(dirname + '/' + shortname + '/' + name_zdb + '.zdb', 'w')
    writer = csv.writer(fichier_zdb)
    writer.writerow(['UE', 'TYPE', 'Z'])
    for datum in data_zdb:
        if len(datum[0]) == 0:
            datum[0] = '--'
        writer.writerow(datum)
    fichier_zdb.close()

# Construction de la fenêtre principale
root = Tk()
root.title('csv2scr_ds.py')

# Configuration de l'apparence du canevas
board = Canvas(root, width=360, height=120, bg='#AC9393')
board.pack(expand=YES, fill=BOTH)
img_png = Image.open('logo_csv2scr.png')
logo = ImageTk.PhotoImage(img_png)
board.update()
board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)

# Options pour les boutons
button_opt = {'fill': tkinter.constants.BOTH, 'padx': 3, 'pady': 3}

# Construction du bouton 'Choix du fichier CSV'
filebut = Button(root, text=filetext, command=openFile)
filebut.pack(**button_opt)

# Construction du bouton 'Répertoire cible'
dirbut = Button(root, text=dirtext, command=openDirectory)
dirbut.pack(**button_opt)

# Construction du bouton 'Exécuter'
exebut = Button(root, text='Exécuter', command=root.quit)
exebut.pack(**button_opt)

# Lancement de la boucle principale
root.mainloop()

# Déclaration des noms des fichiers
filename = filebut['text']
dirname = dirbut['text']
(shortname, ext) = os.path.splitext(os.path.basename(filename))

# Overkill!
root.destroy()

# Ouverture du fichier CSV
fichier_csv = open(filename, 'r')

# Écriture du fichier SCR
fichier_scr = open(dirname + '/' + shortname + '.scr', 'w')

# Création du dossier ZDB
if not os.path.exists(dirname + '/' + shortname):
    os.mkdir(dirname + '/' + shortname)

# Création des calques
fichier_scr.write('-CALQUE\n')
fichier_scr.write('N\n')
fichier_scr.write('1-Tranchées,1-Tranchées_No,2-Structures,2-Structures_No,3-Axes,3-Axes_No,4-Clous,4-Clous_No,4-Clous_Redressement,4-Clous_Redressement_No,5-Pts_Isolés,5-Pts_Isolés_Info,5-Pts_Mobilier,5-Pts_Mobilier_Info,6-TopoAlti,6-TopoCode,6-TopoMat,6-TopoPoint,7-Zones_de_fouille\n')
fichier_scr.write('LC\n')
fichier_scr.write('48\n')
fichier_scr.write('1-Tranchées\n')
fichier_scr.write('LC\n')
fichier_scr.write('jaune\n')
fichier_scr.write('1-Tranchées_No,3-Axes_No,4-Clous_No,4-Clous_Redressement_No,5-Pts_Isolés_Info,5-Pts_Mobilier_Info\n')
fichier_scr.write('LC\n')
fichier_scr.write('magenta\n')
fichier_scr.write('3-Axes\n')
fichier_scr.write('LC\n')
fichier_scr.write('bleu\n')
fichier_scr.write('4-Clous_Redressement\n')
fichier_scr.write('LC\n')
fichier_scr.write('vert\n')
fichier_scr.write('5-Pts_Isolés\n')
fichier_scr.write('LC\n')
fichier_scr.write('rouge\n')
fichier_scr.write('6-TopoAlti\n')
fichier_scr.write('LC\n')
fichier_scr.write('252\n')
fichier_scr.write('6-TopoCode\n')
fichier_scr.write('LC\n')
fichier_scr.write('cyan\n')
fichier_scr.write('6-TopoMat\n')
fichier_scr.write('LC\n')
fichier_scr.write('88\n')
fichier_scr.write('7-Zones_de_fouille\n\n')

# Insertion des points
list_ZDB = []
reader = csv.reader(fichier_csv)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('6-TopoPoint\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    fichier_scr.write('POINT ' + coord_E + ',' + coord_N + ',' + coord_Z + '\n')
    alti = format(float(coord_Z), '.2f')
    info = coord_Z + '|' + code + '|' + mat
    list_ZDB.append([info, 'Point', alti])

# Écriture du fichier ZDB
FileZDB('6-TopoPoint', list_ZDB)

# Insertion des altitudes
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('6-TopoAlti\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.05 0 ' + coord_Z + '\n')

# Insertion des codes
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('6-TopoCode\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + format(float(coord_N) - 0.075, '.3f') + ',' + coord_Z + ' 0.05 0 ' + code + '\n')

# Insertion des matricules
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('6-TopoMat\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + format(float(coord_N) - 0.150, '.3f') + ',' + coord_Z + ' 0.05 0 ' + mat + '\n')

# Insertion des tranchées
list_ZDB = []
list_ENZ = []
list_Z = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('1-Tranchées\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '20':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
    if code == '21':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ) + '\n')
        fichier_scr.write('F\n')
        alti = format(sum(list_Z) / len(list_Z), '.2f')
        list_ZDB.append([info, 'Tranchée', alti])
        list_ENZ = []
        list_Z = []

# Écriture du fichier ZDB
FileZDB('1-Tranchées', list_ZDB)

# Insertion des numéros de tranchées
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('1-Tranchées_No\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if info != '' and code == '21':
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

# Insertion des structures
list_ZDB = []
list_ENZ = []
list_Z = []
adjust_ENZ = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('2-Structures\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '30':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
    if code == '311':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
        adjust_ENZ.append(list_ENZ[0])
        for element in list_ENZ[1:]:
            adjust_ENZ.append(','.join(element.split(',')[:-1]))
        fichier_scr.write('POLYLIGNE ' + ' '.join(adjust_ENZ) + '\n\n')
        fichier_scr.write('EDITPOLYLIGNE\n')
        fichier_scr.write('D\n')
        fichier_scr.write('AJ\n')
        fichier_scr.write('O\n')
        alti = format(sum(list_Z) / len(list_Z), '.2f')
        list_ZDB.append([info, 'Structure', alti])
        list_ENZ = []
        list_Z = []
        adjust_ENZ = []
    if code == '312':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
        adjust_ENZ.append(list_ENZ[0])
        for element in list_ENZ[1:]:
            adjust_ENZ.append(','.join(element.split(',')[:-1]))
        fichier_scr.write('POLYLIGNE ' + ' '.join(adjust_ENZ) + '\n')
        fichier_scr.write('F\n')
        fichier_scr.write('EDITPOLYLIGNE\n')
        fichier_scr.write('D\n')
        fichier_scr.write('AJ\n')
        fichier_scr.write('O\n')
        alti = format(sum(list_Z) / len(list_Z), '.2f')
        list_ZDB.append([info, 'Structure', alti])
        list_ENZ = []
        list_Z = []
        adjust_ENZ = []
    if code == '313':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ) + '\n\n')
        alti = format(sum(list_Z) / len(list_Z), '.2f')
        list_ZDB.append([info, 'Structure', alti])
        list_ENZ = []
        list_Z = []
    if code == '314':
        list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_Z.append(float(coord_Z))
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ) + '\n')
        fichier_scr.write('F\n')
        alti = format(sum(list_Z) / len(list_Z), '.2f')
        list_ZDB.append([info, 'Structure', alti])
        list_ENZ = []
        list_Z = []

# Écriture du fichier ZDB
FileZDB('2-Structures', list_ZDB)

# Insertion des numéros de structures
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('2-Structures_No\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if info != '' and (code == '311' or code == '312' or code == '313' or code == '314'):
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

# Insertion des axes
list_ZDB = []
list_ENZ40 = []
list_ENZ50 = []
list_ENZ60 = []
list_info40 = []
list_info50 = []
list_info60 = []
list_Z40 = []
list_Z50 = []
list_Z60 = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('3-Axes\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]

    # Axes simples
    if code == '40':
        list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info40.append(info)
        list_Z40.append(float(coord_Z))
    if code == '41':
        list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info40.append(info)
        list_Z40.append(float(coord_Z))
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ40) + '\n\n')
        alti = format(sum(list_Z40) / len(list_Z40), '.2f')
        info = list_info40[-1]
        list_ZDB.append([info, 'Axe', alti])
        list_ENZ40 = []
        list_info40 = []
        list_Z40 = []

    # Axes de plans
    if code == '50':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
        list_Z50.append(float(coord_Z))
    if code == '51':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
        list_Z50.append(float(coord_Z))
        fichier_scr.write('-COULEURLIGNE\n')
        fichier_scr.write('rouge\n')
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ50) + '\n\n')
        fichier_scr.write('-COULEURLIGNE\n')
        fichier_scr.write('ByLayer\n')
        alti = format(sum(list_Z50) / len(list_Z50), '.2f')
        if set(list_info50[:-1]) == {''} and list_info50[-1] != '':
            info = list_info50[-1]
            list_ZDB.append([info, 'Axe_Plan', alti])
        else:
            info = ''
            list_ZDB.append([info, 'Axe_Plan', alti])
        list_ENZ50 = []
        list_info50 = []
        list_Z50 = []

    # Axes de coupes
    if code == '60':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
        list_Z60.append(float(coord_Z))
    if code == '61':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
        list_Z60.append(float(coord_Z))
        fichier_scr.write('-COULEURLIGNE\n')
        fichier_scr.write('30\n')
        fichier_scr.write('POLYLIGNE3D ' + ' '.join(list_ENZ60) + '\n\n')
        fichier_scr.write('-COULEURLIGNE\n')
        fichier_scr.write('ByLayer\n')
        alti = format(sum(list_Z60) / len(list_Z60), '.2f')
        if set(list_info60[:-1]) == {''} and list_info60[-1] != '':
            info = list_info60[-1]
            list_ZDB.append([info, 'Axe_Coupe', alti])
        else:
            info = ''
            list_ZDB.append([info, 'Axe_Coupe', alti])
        list_ENZ60 = []
        list_info60 = []
        list_Z60 = []

# Écriture du fichier ZDB
FileZDB('3-Axes', list_ZDB)

# Insertion des numéros d'axes
list_ENZ40 = []
list_ENZ50 = []
list_ENZ60 = []
list_info40 = []
list_info50 = []
list_info60 = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('3-Axes_No\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]

    # Numéros d'axes simples
    if code == '40':
        list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info40.append(info)
    if code == '41':
        list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info40.append(info)
        if info != '':
            for num in range(len(list_ENZ40))[:-1]:
                InsNumAxes(list_ENZ40[num], list_ENZ40[num + 1], list_info40[-1])
        list_ENZ40 = []
        list_info40 = []

    # Numéros d'axes de plans
    if code == '50':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
    if code == '51':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
        if set(list_info50[:-1]) == {''} and list_info50[-1] != '':
            for num in range(len(list_ENZ50))[:-1]:
                InsNumAxes(list_ENZ50[num], list_ENZ50[num + 1], list_info50[-1])
        list_ENZ50 = []
        list_info50 = []

    # Numéros d'axes de coupes
    if code == '60':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
    if code == '61':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
        if set(list_info60[:-1]) == {''} and list_info60[-1] != '':
            for num in range(len(list_ENZ60))[:-1]:
                InsNumAxes(list_ENZ60[num], list_ENZ60[num + 1], list_info60[-1])
        list_ENZ60 = []
        list_info60 = []

# Insertion des clous
list_ZDB = []
list_ENZ50 = []
list_ENZ60 = []
list_info50 = []
list_info60 = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('4-Clous\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '11':
        fichier_scr.write('-INSERBLOC 11-clou.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Clou', alti])
    if code == '50':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
    if code == '51':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
        if len(set(list_info50)) == len(list_info50) and '' not in list_info50:
            for num, coord_ENZ in enumerate(list_ENZ50):
                fichier_scr.write('-INSERBLOC 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                info = list_info50[num]
                list_ZDB.append([info, 'Clou', alti])
        else:
            for coord_ENZ in list_ENZ50:
                fichier_scr.write('-INSERBLOC 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                info = ''
                list_ZDB.append([info, 'Clou', alti])
        list_ENZ50 = []
        list_info50 = []
    if code == '60':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
    if code == '61':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
        if len(set(list_info60)) == len(list_info60) and '' not in list_info60:
            for num, coord_ENZ in enumerate(list_ENZ60):
                fichier_scr.write('-INSERBLOC 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                info = list_info60[num]
                list_ZDB.append([info, 'Clou', alti])
        else:
            for coord_ENZ in list_ENZ60:
                fichier_scr.write('-INSERBLOC 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                info = ''
                list_ZDB.append([info, 'Clou', alti])
        list_ENZ60 = []
        list_info60 = []

# Écriture du fichier ZDB
FileZDB('4-Clous', list_ZDB)

# Insertion des numéros de clous
list_ENZ50 = []
list_ENZ60 = []
list_info50 = []
list_info60 = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('4-Clous_No\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '11' and info != '':
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')
    if code == '50':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
    if code == '51':
        list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info50.append(info)
        if len(set(list_info50)) == len(list_info50) and '' not in list_info50:
            for num, coord_ENZ in enumerate(list_ENZ50):
                info = list_info50[num]
                fichier_scr.write('-NOTESIMPLE ' + coord_ENZ + ' 0.1 0 ' + info + '\n')
        list_ENZ50 = []
        list_info50 = []
    if code == '60':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
    if code == '61':
        list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
        list_info60.append(info)
        if len(set(list_info60)) == len(list_info60) and '' not in list_info60:
            for num, coord_ENZ in enumerate(list_ENZ60):
                info = list_info60[num]
                fichier_scr.write('-NOTESIMPLE ' + coord_ENZ + ' 0.1 0 ' + info + '\n')
        list_ENZ60 = []
        list_info60 = []

# Insertion des clous de redressement
list_ZDB = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('4-Clous_Redressement\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '12':
        fichier_scr.write('-INSERBLOC 12-clou_redressement.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Clou_Redressement', alti])

# Écriture du fichier ZDB
FileZDB('4-Clous_Redressement', list_ZDB)

# Insertion des numéros de clous de redressement
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('4-Clous_Redressement_No\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if info != '' and code == '12':
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

# Insertion des points isolés
list_ZDB = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('5-Pts_Isolés\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '10':
        fichier_scr.write('-INSERBLOC 10-pt_topo.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Point_Isolé', alti])
    if code == '19':
        fichier_scr.write('-INSERBLOC 19-drapeau.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Drapeau', alti])

# Écriture du fichier ZDB
FileZDB('5-Pts_Isolés', list_ZDB)

# Insertion des infos des points isolés
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('5-Pts_Isolés_Info\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if info != '' and (code == '10' or code == '19'):
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

# Insertion des points mobilier
list_ZDB = []
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('5-Pts_Mobilier\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if code == '13':
        fichier_scr.write('-INSERBLOC 13-pt_ceramique.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Céramique', alti])
    if code == '14':
        fichier_scr.write('-INSERBLOC 14-pt_lithique.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Lithique', alti])
    if code == '15':
        fichier_scr.write('-INSERBLOC 15-pt_metal.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Métal', alti])
    if code == '16':
        fichier_scr.write('-INSERBLOC 16-pt_monnaie.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Monnaie', alti])
    if code == '17':
        fichier_scr.write('-INSERBLOC 17-pt_borne.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Borne', alti])
    if code == '18':
        fichier_scr.write('-INSERBLOC 18-pt_munition.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
        alti = format(float(coord_Z), '.2f')
        list_ZDB.append([info, 'Munition', alti])

# Écriture du fichier ZDB
FileZDB('5-Pts_Mobilier', list_ZDB)

# Insertion des infos des points mobilier
fichier_csv.seek(0)
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('5-Pts_Mobilier_Info\n\n')
for row in reader:
    mat = row[0]
    coord_E = row[1]
    coord_N = row[2]
    coord_Z = row[3]
    code = row[4]
    info = row[5]
    if info != '' and (code == '13' or code == '14' or code == '15' or code == '16' or code == '17' or code == '18'):
        fichier_scr.write('-NOTESIMPLE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

# Retour au calque 0
fichier_scr.write('-CALQUE\n')
fichier_scr.write('ET\n')
fichier_scr.write('0\n\n')

# Zoom sur l'ensemble du dessin
fichier_scr.write('ZOOM\n')
fichier_scr.write('AJ\n')

# Fermeture des fichiers SCR et CSV
fichier_scr.close()
fichier_csv.close()
