#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
csv2scr.py
Ce programme convertit un fichier CSV en script SCR pour AutoCAD.
Licence GPLv3
https://www.gnu.org/licenses/gpl-3.0.fr.html
2019/10/24
Jean-Charles Braun
'''

from tkinter import Tk, Canvas, Button, CENTER, YES, BOTH, Label, Entry, X, LEFT, RIGHT, LabelFrame, Frame
from PIL import Image, ImageTk
import tkinter.constants
import tkinter.filedialog
import tkinter.messagebox
import os
import csv
import math

# Définition des variables
filetext = 'Choix du fichier CSV'
dirtext = 'Répertoire cible'
XD_set = 1
push = 0

def openFile():
    global push
    global filename
    if push == 0:
        directory = '~'
    elif push == 1:
        directory = os.path.dirname(str(filename))
    filename = tkinter.filedialog.askopenfilename(parent=root, initialdir=directory, title=filetext, filetypes=[('CSV files', '.csv'), ('All files', '*')])
    if bool(filename):
        filebut['text'] = str(filename)
    else:
        filebut['text'] = filetext
    board.delete('all')
    board.update()
    board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)
    push = 1

def openDirectory():
    dirname = tkinter.filedialog.askdirectory(parent=root, initialdir='~', title=dirtext)
    if bool(dirname):
        dirbut['text'] = str(dirname)
    else:
        dirbut['text'] = dirtext
    board.delete('all')
    board.update()
    board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)

def enable_xd():
    global XD_set
    Ent1.configure(state='normal')
    Ent2.configure(state='normal')
    Ent3.configure(state='normal')
    XD_set = 1

def disable_xd():
    global XD_set
    Ent1.configure(state='disabled')
    Ent2.configure(state='disabled')
    Ent3.configure(state='disabled')
    XD_set = 0

def askquit():
    if tkinter.messagebox.askokcancel('Quitter', 'Voulez-vous quitter ?'):
        root.quit()

def goconvert():
    date_ue = Ent1.get()
    commune = Ent2.get()
    num_op = Ent3.get()

    def X_DATA(zmoy_ue, type_ue, num_ue):
        fichier_scr.write('XDATA\n')
        fichier_scr.write('D\n')
        fichier_scr.write('TABLE_UE\n')
        fichier_scr.write('ST ' + num_op + '\n')
        fichier_scr.write('ST ' + commune + '\n')
        fichier_scr.write('ST ' + date_ue + '\n')
        fichier_scr.write('ST ' + zmoy_ue + '\n')
        fichier_scr.write('ST ' + type_ue + '\n')
        if len(num_ue) == 0:
            num_ue = '--'
        fichier_scr.write('ST ' + num_ue + '\n')
        fichier_scr.write('X\n')

    def InsNumAxes(startpoint, endpoint, info):
        x1 = float(startpoint.split(',')[0])
        x2 = float(endpoint.split(',')[0])
        y1 = float(startpoint.split(',')[1])
        y2 = float(endpoint.split(',')[1])
        z1 = float(startpoint.split(',')[2])
        z2 = float(endpoint.split(',')[2])
        midpoint_x = format((x1 + x2) / 2, '.3f')
        midpoint_y = format((y1 + y2) / 2, '.3f')
        midpoint_z = format((z1 + z2) / 2, '.3f')
        arctan = math.atan((y2 - y1) / (x2 - x1))
        angle = format(math.degrees(arctan), '.3f')
        fichier_scr.write('-TEXTE ' + midpoint_x + ',' + midpoint_y + ',' + midpoint_z + ' 0.05 ' + angle + ' ' + info + '\n')

    # Déclaration des noms des fichiers
    filename = filebut['text']
    dirname = dirbut['text']
    (shortname, ext) = os.path.splitext(os.path.basename(filename))

    # Ouverture du fichier CSV
    fichier_csv = open(filename, 'r')

    # Écriture du fichier SCR
    fichier_scr = open(dirname + '/' + shortname + '.scr', 'w')

    # Création des calques
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('N\n')
    fichier_scr.write('1-Tranchées,1-Tranchées_No,2-Structures,2-Structures_No,3-Axes,3-Axes_No,4-Clous,4-Clous_No,4-Clous_Redressement,4-Clous_Redressement_No,5-Pts_Isolés,5-Pts_Isolés_Info,5-Pts_Mobilier,5-Pts_Mobilier_Info,6-TopoAlti,6-TopoCode,6-TopoMat,6-TopoPoint,7-Zones_de_fouille\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('48\n')
    fichier_scr.write('1-Tranchées\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('jaune\n')
    fichier_scr.write('1-Tranchées_No,3-Axes_No,4-Clous_No,4-Clous_Redressement_No,5-Pts_Isolés_Info,5-Pts_Mobilier_Info\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('magenta\n')
    fichier_scr.write('3-Axes\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('bleu\n')
    fichier_scr.write('4-Clous_Redressement\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('vert\n')
    fichier_scr.write('5-Pts_Isolés\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('rouge\n')
    fichier_scr.write('6-TopoAlti\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('252\n')
    fichier_scr.write('6-TopoCode\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('cyan\n')
    fichier_scr.write('6-TopoMat\n')
    fichier_scr.write('CO\n')
    fichier_scr.write('88\n')
    fichier_scr.write('7-Zones_de_fouille\n\n')

    # Insertion des points
    reader = csv.reader(fichier_csv)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('6-TopoPoint\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        fichier_scr.write('POINT ' + coord_E + ',' + coord_N + ',' + coord_Z + '\n')
        # Insertion des XDATA
        if XD_set == 1:
            alti = format(float(coord_Z), '.2f')
            info = coord_Z + '|' + code + '|' + mat
            X_DATA(alti, 'Point', info)

    # Insertion des altitudes
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('6-TopoAlti\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.05 0 ' + coord_Z + '\n')

    # Insertion des codes
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('6-TopoCode\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        fichier_scr.write('-TEXTE ' + coord_E + ',' + format(float(coord_N) - 0.075, '.3f') + ',' + coord_Z + ' 0.05 0 ' + code + '\n')

    # Insertion des matricules
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('6-TopoMat\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        fichier_scr.write('-TEXTE ' + coord_E + ',' + format(float(coord_N) - 0.150, '.3f') + ',' + coord_Z + ' 0.05 0 ' + mat + '\n')

    # Insertion des tranchées
    list_ENZ = []
    list_Z = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('1-Tranchées\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '20':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
        if code == '21':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ) + '\n')
            fichier_scr.write('C\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z) / len(list_Z), '.2f')
                X_DATA(alti, 'Tranchée', info)
            list_ENZ = []
            list_Z = []

    # Insertion des numéros de tranchées
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('1-Tranchées_No\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if info != '' and code == '21':
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

    # Insertion des structures
    list_ENZ = []
    list_Z = []
    adjust_ENZ = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('2-Structures\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '30':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
        if code == '311':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
            adjust_ENZ.append(list_ENZ[0])
            for element in list_ENZ[1:]:
                adjust_ENZ.append(','.join(element.split(',')[:-1]))
            fichier_scr.write('POLYLIGN ' + ' '.join(adjust_ENZ) + '\n\n')
            fichier_scr.write('PEDIT\n')
            fichier_scr.write('D\n')
            fichier_scr.write('A\n')
            fichier_scr.write('Q\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z) / len(list_Z), '.2f')
                X_DATA(alti, 'Structure', info)
            list_ENZ = []
            list_Z = []
            adjust_ENZ = []
        if code == '312':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
            adjust_ENZ.append(list_ENZ[0])
            for element in list_ENZ[1:]:
                adjust_ENZ.append(','.join(element.split(',')[:-1]))
            fichier_scr.write('POLYLIGN ' + ' '.join(adjust_ENZ) + '\n')
            fichier_scr.write('C\n')
            fichier_scr.write('PEDIT\n')
            fichier_scr.write('D\n')
            fichier_scr.write('A\n')
            fichier_scr.write('Q\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z) / len(list_Z), '.2f')
                X_DATA(alti, 'Structure', info)
            list_ENZ = []
            list_Z = []
            adjust_ENZ = []
        if code == '313':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ) + '\n\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z) / len(list_Z), '.2f')
                X_DATA(alti, 'Structure', info)
            list_ENZ = []
            list_Z = []
        if code == '314':
            list_ENZ.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_Z.append(float(coord_Z))
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ) + '\n')
            fichier_scr.write('C\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z) / len(list_Z), '.2f')
                X_DATA(alti, 'Structure', info)
            list_ENZ = []
            list_Z = []

    # Insertion des numéros de structures
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('2-Structures_No\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if info != '' and (code == '311' or code == '312' or code == '313' or code == '314'):
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

    # Insertion des axes
    list_ENZ40 = []
    list_ENZ50 = []
    list_ENZ60 = []
    list_info40 = []
    list_info50 = []
    list_info60 = []
    list_Z40 = []
    list_Z50 = []
    list_Z60 = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('3-Axes\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]

        # Axes simples
        if code == '40':
            list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info40.append(info)
            list_Z40.append(float(coord_Z))
        if code == '41':
            list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info40.append(info)
            list_Z40.append(float(coord_Z))
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ40) + '\n\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z40) / len(list_Z40), '.2f')
                info = list_info40[-1]
                X_DATA(alti, 'Axe', info)
            list_ENZ40 = []
            list_info40 = []
            list_Z40 = []

        # Axes de plans
        if code == '50':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
            list_Z50.append(float(coord_Z))
        if code == '51':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
            list_Z50.append(float(coord_Z))
            fichier_scr.write('-COULEUR\n')
            fichier_scr.write('rouge\n')
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ50) + '\n\n')
            fichier_scr.write('-COULEUR\n')
            fichier_scr.write('ByLayer\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z50) / len(list_Z50), '.2f')
                if set(list_info50[:-1]) == {''} and list_info50[-1] != '':
                    info = list_info50[-1]
                    X_DATA(alti, 'Axe_Plan', info)
                else:
                    info = ''
                    X_DATA(alti, 'Axe_Plan', info)
            list_ENZ50 = []
            list_info50 = []
            list_Z50 = []

        # Axes de coupes
        if code == '60':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
            list_Z60.append(float(coord_Z))
        if code == '61':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
            list_Z60.append(float(coord_Z))
            fichier_scr.write('-COULEUR\n')
            fichier_scr.write('30\n')
            fichier_scr.write('POLY3D ' + ' '.join(list_ENZ60) + '\n\n')
            fichier_scr.write('-COULEUR\n')
            fichier_scr.write('ByLayer\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(sum(list_Z60) / len(list_Z60), '.2f')
                if set(list_info60[:-1]) == {''} and list_info60[-1] != '':
                    info = list_info60[-1]
                    X_DATA(alti, 'Axe_Coupe', info)
                else:
                    info = ''
                    X_DATA(alti, 'Axe_Coupe', info)
            list_ENZ60 = []
            list_info60 = []
            list_Z60 = []

    # Insertion des numéros d'axes
    list_ENZ40 = []
    list_ENZ50 = []
    list_ENZ60 = []
    list_info40 = []
    list_info50 = []
    list_info60 = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('3-Axes_No\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]

        # Numéros d'axes simples
        if code == '40':
            list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info40.append(info)
        if code == '41':
            list_ENZ40.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info40.append(info)
            if info != '':
                for num in range(len(list_ENZ40))[:-1]:
                    InsNumAxes(list_ENZ40[num], list_ENZ40[num + 1], list_info40[-1])
            list_ENZ40 = []
            list_info40 = []

        # Numéros d'axes de plans
        if code == '50':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
        if code == '51':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
            if set(list_info50[:-1]) == {''} and list_info50[-1] != '':
                for num in range(len(list_ENZ50))[:-1]:
                    InsNumAxes(list_ENZ50[num], list_ENZ50[num + 1], list_info50[-1])
            list_ENZ50 = []
            list_info50 = []

        # Numéros d'axes de coupes
        if code == '60':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
        if code == '61':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
            if set(list_info60[:-1]) == {''} and list_info60[-1] != '':
                for num in range(len(list_ENZ60))[:-1]:
                    InsNumAxes(list_ENZ60[num], list_ENZ60[num + 1], list_info60[-1])
            list_ENZ60 = []
            list_info60 = []

    # Insertion des clous
    list_ENZ50 = []
    list_ENZ60 = []
    list_info50 = []
    list_info60 = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('4-Clous\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '11':
            fichier_scr.write('-INSERER 11-clou.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Clou', info)
        if code == '50':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
        if code == '51':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
            if len(set(list_info50)) == len(list_info50) and '' not in list_info50:
                for num, coord_ENZ in enumerate(list_ENZ50):
                    fichier_scr.write('-INSERER 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                    # Insertion des XDATA
                    if XD_set == 1:
                        alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                        info = list_info50[num]
                        X_DATA(alti, 'Clou', info)
            else:
                for coord_ENZ in list_ENZ50:
                    fichier_scr.write('-INSERER 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                    # Insertion des XDATA
                    if XD_set == 1:
                        alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                        info = ''
                        X_DATA(alti, 'Clou', info)
            list_ENZ50 = []
            list_info50 = []
        if code == '60':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
        if code == '61':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
            if len(set(list_info60)) == len(list_info60) and '' not in list_info60:
                for num, coord_ENZ in enumerate(list_ENZ60):
                    fichier_scr.write('-INSERER 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                    # Insertion des XDATA
                    if XD_set == 1:
                        alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                        info = list_info60[num]
                        X_DATA(alti, 'Clou', info)
            else:
                for coord_ENZ in list_ENZ60:
                    fichier_scr.write('-INSERER 11-clou.dxf ' + coord_ENZ + ' 1 1 0\n')
                    # Insertion des XDATA
                    if XD_set == 1:
                        alti = format(float(coord_ENZ.split(',')[2]), '.2f')
                        info = ''
                        X_DATA(alti, 'Clou', info)
            list_ENZ60 = []
            list_info60 = []

    # Insertion des numéros de clous
    list_ENZ50 = []
    list_ENZ60 = []
    list_info50 = []
    list_info60 = []
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('4-Clous_No\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '11' and info != '':
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')
        if code == '50':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
        if code == '51':
            list_ENZ50.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info50.append(info)
            if len(set(list_info50)) == len(list_info50) and '' not in list_info50:
                for num, coord_ENZ in enumerate(list_ENZ50):
                    info = list_info50[num]
                    fichier_scr.write('-TEXTE ' + coord_ENZ + ' 0.1 0 ' + info + '\n')
            list_ENZ50 = []
            list_info50 = []
        if code == '60':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
        if code == '61':
            list_ENZ60.append(coord_E + ',' + coord_N + ',' + coord_Z)
            list_info60.append(info)
            if len(set(list_info60)) == len(list_info60) and '' not in list_info60:
                for num, coord_ENZ in enumerate(list_ENZ60):
                    info = list_info60[num]
                    fichier_scr.write('-TEXTE ' + coord_ENZ + ' 0.1 0 ' + info + '\n')
            list_ENZ60 = []
            list_info60 = []

    # Insertion des clous de redressement
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('4-Clous_Redressement\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '12':
            fichier_scr.write('-INSERER 12-clou_redressement.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Clou_Redressement', info)

    # Insertion des numéros de clous de redressement
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('4-Clous_Redressement_No\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if info != '' and code == '12':
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

    # Insertion des points isolés
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Isolés\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '10':
            fichier_scr.write('-INSERER 10-pt_topo.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Point_Isolé', info)
        if code == '19':
            fichier_scr.write('-INSERER 19-drapeau.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Drapeau', info)

    # Insertion des infos des points isolés
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Isolés_Info\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if info != '' and (code == '10' or code == '19'):
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

    # Insertion des points mobilier
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Mobilier\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code == '13':
            fichier_scr.write('-INSERER 13-pt_ceramique.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Céramique', info)
        if code == '14':
            fichier_scr.write('-INSERER 14-pt_lithique.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Lithique', info)
        if code == '15':
            fichier_scr.write('-INSERER 15-pt_metal.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Métal', info)
        if code == '16':
            fichier_scr.write('-INSERER 16-pt_monnaie.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Monnaie', info)
        if code == '17':
            fichier_scr.write('-INSERER 17-pt_borne.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Borne', info)
        if code == '18':
            fichier_scr.write('-INSERER 18-pt_munition.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                X_DATA(alti, 'Munition', info)

    # Insertion des infos des points mobilier
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Mobilier_Info\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if info != '' and (code == '13' or code == '14' or code == '15' or code == '16' or code == '17' or code == '18'):
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + info + '\n')

    # Dictionnaire des codes SRA
    register = {'1000': 'RXX', '1010': 'RLA', '1020': 'RLT', '1030': 'RFS', '1040': 'RLG', '2000': 'TXX', '2010': 'TCR', '2020': 'TCU', '2030': 'TCA', '2040': 'TAC', '3000': 'OXX', '3010': 'OHU', '3020': 'OFA', '3030': 'OIC', '3040': 'OMA', '3050': 'OTR', '3060': 'OCQ', '4000': 'GXX', '4010': 'GBS', '4020': 'GTX', '4030': 'GCP', '5000': 'MXX', '5010': 'MOR', '5020': 'MAG', '5030': 'MCU', '5040': 'MFE', '5050': 'MPB', '5060': 'MSN', '5070': 'MZN', '5080': 'MMO', '6000': 'CXX', '6010': 'CEM', '6020': 'CMQ', '6030': 'CEP', '6040': 'CMP', '6050': 'CIN', '7000': 'PXX', '7010': 'PAN', '7020': 'PCA', '7030': 'PPA', '7040': 'PGR', '7050': 'PMM', '7060': 'PDD', '7070': 'PTL', '7080': 'P14', '7090': 'PPO', '7100': 'PPR', '8000': 'WTF', '8010': 'ZXX', '8020': 'VXX', '8030': 'SXX', '8040': 'FXX'}

    # Insertion des points mobilier (codes SRA)
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Mobilier\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code in register:
            mobx = register[code]
            fichier_scr.write('-INSERER 10-pt_topo.dxf ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 1 1 0\n')
            # Insertion des XDATA
            if XD_set == 1:
                alti = format(float(coord_Z), '.2f')
                codx = num_op + '-' + mobx + '-' + info
                X_DATA(alti, mobx, codx)

    # Insertion des infos des points mobilier (codes SRA)
    fichier_csv.seek(0)
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('5-Pts_Mobilier_Info\n\n')
    for row in reader:
        mat = row[0]
        coord_E = row[1]
        coord_N = row[2]
        coord_Z = row[3]
        code = row[4]
        info = row[5]
        if code in register:
            mobx = register[code]
            fichier_scr.write('-TEXTE ' + coord_E + ',' + coord_N + ',' + coord_Z + ' 0.1 0 ' + num_op + '-' + mobx + '-' + info + '\n')

    # Retour au calque 0
    fichier_scr.write('-CALQUE\n')
    fichier_scr.write('CH\n')
    fichier_scr.write('0\n\n')

    # Zoom sur l'ensemble du dessin
    fichier_scr.write('ZOOM\n')
    fichier_scr.write('ET\n')

    # Fermeture des fichiers SCR et CSV
    fichier_scr.close()
    fichier_csv.close()

    # Affichage d'un message à la fin du traitement
    tkinter.messagebox.showinfo('Résultat', 'Fichier SCR créé avec succès !')

# Construction de la fenêtre principale
root = Tk()
root.title('csv2scr.py')

# Configuration de l'apparence du canevas
board = Canvas(root, width=360, height=120, bg='#AC9393')
board.pack(expand=YES, fill=BOTH)
img_png = Image.open('logo_csv2scr.png')
logo = ImageTk.PhotoImage(img_png)
board.update()
board.create_image(board.winfo_width() / 2, board.winfo_height() / 2, anchor=CENTER, image=logo)

# Options pour les boutons et les frames
button_opt = {'fill': tkinter.constants.BOTH, 'padx': 3, 'pady': 3}
frame_opt = {'fill': tkinter.constants.X, 'padx': 5, 'pady': 5}

# Construction du bouton 'Choix du fichier CSV'
filebut = Button(root, text=filetext, command=openFile)
filebut.pack(**button_opt)

# Frame principale
labelframe = LabelFrame(root)
labelframe.pack(**frame_opt)

# Frame 'XDATA'
Frm0 = Frame(labelframe)
Frm0.pack(**frame_opt)
Lbl0 = Label(Frm0, width=17, text='Ajout XDATA')
Lbl0.pack(side=LEFT)
FrmXD = Frame(Frm0, width=23)
FrmXD.pack(side=RIGHT)
activ1 = Button(FrmXD, text='Activé', command=enable_xd, width=7, bd=3, fg='green')
activ1.pack(side=LEFT)
blank = Label(FrmXD, text='/', bd=6)
blank.pack(side=LEFT)
activ0 = Button(FrmXD, text='Désactivé', command=disable_xd, width=7, bd=3, fg='red')
activ0.pack(side=LEFT)

# Frame 'Date Levé'
Frm1 = Frame(labelframe)
Frm1.pack(**frame_opt)
Lbl1 = Label(Frm1, width=17, text='Date Levé :')
Lbl1.pack(side=LEFT)
Ent1 = Entry(Frm1, width=23, bd=3)
Ent1.pack(side=RIGHT)

# Frame 'Commune'
Frm2 = Frame(labelframe)
Frm2.pack(**frame_opt)
Lbl2 = Label(Frm2, width=17, text='Commune :')
Lbl2.pack(side=LEFT)
Ent2 = Entry(Frm2, width=23, bd=3)
Ent2.pack(side=RIGHT)

# Frame 'Numéro OP'
Frm3 = Frame(labelframe)
Frm3.pack(**frame_opt)
Lbl3 = Label(Frm3, width=17, text='Numéro OP :')
Lbl3.pack(side=LEFT)
Ent3 = Entry(Frm3, width=23, bd=3)
Ent3.pack(side=RIGHT)

# Construction du bouton 'Répertoire cible'
dirbut = Button(root, text=dirtext, command=openDirectory)
dirbut.pack(**button_opt)

# Construction des boutons 'Quitter' et 'Exécuter'
Frm4 = Frame(root)
Frm4.pack(**button_opt)
quitbut = Button(Frm4, text='Quitter', command=root.quit, width=14)
quitbut.pack(side=LEFT)
exebut = Button(Frm4, text='Exécuter', command=goconvert, width=22)
exebut.pack(side=RIGHT)

# Interaction avec le gestionnaire de fenêtre
root.protocol('WM_DELETE_WINDOW', askquit)

# Lancement de la boucle principale
root.mainloop()

# Overkill!
root.destroy()
