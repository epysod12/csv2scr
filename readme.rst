===================
csv2scr pour Python
===================

Introduction
------------

Ce programme convertit un fichier CSV en script SCR pour DraftSight ou AutoCAD.

Nouveautés
----------

- adaptation des commandes pour AutoCAD
- ajout des XDATA (*Extended Entity Data*) sur les tracés, les blocs et les points
- ajout des entrées 'Date Levé', 'Commune' et 'Numéro OP'
- extension de la codification pour le mobilier et les prélèvements
- persistance de la fenêtre après l'exécution du programme
- ajout d'une confirmation lors de la fermeture de la fenêtre
- ajout d'un message ``Fichier SCR créé avec succès !``
- ajout d'un bouton ``Quitter``

Prérequis
---------

- python3-tkinter
- python3-pillow
- python3-pillow-tk

Utilisation
-----------

Pour générer un script SCR utilisable sous DraftSight :

    ``$ python3 csv2scr_ds.py``

Pour générer un script SCR utilisable sous AutoCAD :

    ``$ python3 csv2scr.py``

Fichiers DXF
~~~~~~~~~~~~

Le dossier ``dxf`` contient les éléments suivants : ::

    10-pt_topo.dxf
    11-clou.dxf
    12-clou_redressement.dxf
    13-pt_ceramique.dxf
    14-pt_lithique.dxf
    15-pt_metal.dxf
    16-pt_monnaie.dxf
    17-pt_borne.dxf
    18-pt_munition.dxf
    19-drapeau.dxf

Ces fichiers doivent être placés dans le dossier ``My Drawings`` (pour DraftSight) ou dans le dossier ``Mes documents`` (pour AutoCAD). Ces dessins sont utilisés comme des blocs (via la commande -INSERBLOC ou -INSERER) lors de la génération du plan.

L'échelle d'insertion doit être en millimètre pour que les blocs s'insèrent à la bonne échelle. Dans DraftSight, il faut charger le gabarit ``standardiso.dwt``. Dans AutoCAD, le gabarit adéquat se nomme ``acadiso.dwt``.

Fichier XDATA.LSP
~~~~~~~~~~~~~~~~~

Dans AutoCAD, la fonction XDATA est accessible soit depuis les **Express Tools**, soit en copiant le fichier XDATA.LSP (ci-joint) dans le répertoire de base AutoCAD. DraftSight ne permettant pas l'exécution des fichiers LISP, cette fonction est donc réservée à AutoCAD.

Fonctionnement
--------------

Fichier CSV :
~~~~~~~~~~~~~

Le fichier CSV doit être composé de 6 colonnes et les champs séparés par des virgules. Ce séparateur est obligatoire après le champ ``Code``, même si le champ ``Info`` qui le suit est vide.

Exemple :

+-----------+-------------+-------------+---------+------+------+
| Matricule | Coord_E     | Coord_N     | Coord_Z | Code | Info |
+===========+=============+=============+=========+======+======+
|| 1027     || 948228.724 || 149011.850 || 256.91 || 20  ||     |
|| 1028     || 948228.201 || 149013.559 || 256.68 || 20  ||     |
|| 1029     || 948220.785 || 149012.419 || 256.58 || 20  ||     |
|| 1030     || 948220.221 || 149014.224 || 256.66 || 20  ||     |
|| 1031     || 948212.985 || 149013.344 || 256.67 || 20  ||     |
|| 1032     || 948213.430 || 149010.464 || 256.59 || 20  ||     |
|| 1033     || 948207.605 || 149009.167 || 256.72 || 20  ||     |
|| 1034     || 948207.904 || 149007.376 || 256.70 || 21  || 2   |
+-----------+-------------+-------------+---------+------+------+

Fichier SCR :
~~~~~~~~~~~~~

Un fichier SCR est un script de commandes que l'on peut charger dans DraftSight ou AutoCAD. Il est possible de créer des calques, dessiner des polylignes, écrire du texte ou encore insérer des blocs.

Exemple :

+------------------------------------------------------------------------+
| Commandes pour DraftSight                                              |
+========================================================================+
|| -CALQUE                                                               |
|| ET                                                                    |
|| 1-Tranchées                                                           |
||                                                                       |
|| POLYLIGNE3D 948228.724,149011.850,256.91 948228.201,149013.559,256.68 |
|| 948220.785,149012.419,256.58 948220.221,149014.224,256.66             |
|| 948212.985,149013.344,256.67 948213.430,149010.464,256.59             |
|| 948207.605,149009.167,256.72 948207.904,149007.376,256.70             |
|| F                                                                     |
|| -CALQUE                                                               |
|| ET                                                                    |
|| 1-Tranchées_No                                                        |
||                                                                       |
|| -NOTESIMPLE 948207.904,149007.376,256.70 0.1 0 2                      |
+------------------------------------------------------------------------+

Codification
------------

+-------+--------------------+----+-------+--------------------+----+-------+--------------------+
| Code  | Signification      |    | Code  | Signification      |    | Code  | Signification      |
+=======+====================+====+=======+====================+====+=======+====================+
|| 1    || station           |    || 1000 || RXX               |    || 6000 || CXX               |
||      ||                   |    || 1010 || RLA               |    || 6010 || CEM               |
|| 10   || pt topo           |    || 1020 || RLT               |    || 6020 || CMQ               |
|| 11   || clou              |    || 1030 || RFS               |    || 6030 || CEP               |
|| 12   || clou redressement |    || 1040 || RLG               |    || 6040 || CMP               |
|| 13   || pt céramique      |    || 2000 || TXX               |    || 6050 || CIN               |
|| 14   || pt lithique       |    || 2010 || TCR               |    || 7000 || PXX               |
|| 15   || pt métal          |    || 2020 || TCU               |    || 7010 || PAN               |
|| 16   || pt monnaie        |    || 2030 || TCA               |    || 7020 || PCA               |
|| 17   || pt borne          |    || 2040 || TAC               |    || 7030 || PPA               |
|| 18   || pt munition       |    || 3000 || OXX               |    || 7040 || PGR               |
|| 19   || drapeau           |    || 3010 || OHU               |    || 7050 || PMM               |
||      ||                   |    || 3020 || OFA               |    || 7060 || PDD               |
|| 20   || point tranchée    |    || 3030 || OIC               |    || 7070 || PTL               |
|| 21   || fin tranchée      |    || 3040 || OMA               |    || 7080 || P14               |
||      ||                   |    || 3050 || OTR               |    || 7090 || PPO               |
|| 30   || point structure   |    || 3060 || OCQ               |    || 7100 || PPR               |
|| 311  || courbe ouverte    |    || 4000 || GXX               |    || 8000 || WTF               |
|| 312  || courbe fermée     |    || 4010 || GBS               |    || 8010 || ZXX               |
|| 313  || ligne ouverte     |    || 4020 || GTX               |    || 8020 || VXX               |
|| 314  || ligne fermée      |    || 4030 || GCP               |    || 8030 || SXX               |
||      ||                   |    || 5000 || MXX               |    || 8040 || FXX               |
|| 40   || point axe         |    || 5010 || MOR               |    ||      ||                   |
|| 41   || fin axe           |    || 5020 || MAG               |    ||      ||                   |
||      ||                   |    || 5030 || MCU               |    ||      ||                   |
|| 50   || point axe plan    |    || 5040 || MFE               |    ||      ||                   |
|| 51   || fin axe plan      |    || 5050 || MPB               |    ||      ||                   |
||      ||                   |    || 5060 || MSN               |    ||      ||                   |
|| 60   || point axe coupe   |    || 5070 || MZN               |    ||      ||                   |
|| 61   || fin axe coupe     |    || 5080 || MMO               |    ||      ||                   |
+-------+--------------------+----+-------+--------------------+----+-------+--------------------+

Licence
-------

Les programmes ci-joints sont publiés sous licence GPLv3 : ::

    csv2scr_ds.py
    csv2scr.py

https://www.gnu.org/licenses/gpl-3.0.fr.html
